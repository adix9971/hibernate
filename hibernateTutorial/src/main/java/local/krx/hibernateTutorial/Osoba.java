package local.krx.hibernateTutorial;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Osoba {
 
 @Id
 @GeneratedValue
 private int id;
 private String firstName;
 private String lastName;
 private String maidenName;
 private String position;
 private String login;
 private String indexNumber;
 private String departmentName;
 private String linkedinLogin;
 private String studentStatus;
 
 
 
 public int getId() {
  return id;
 }
 public void setId(int id) {
  this.id = id;
 }
 public String getFirstName(){
  return firstName;
 }
 public void setFirstName(String first_name) {
  this.firstName = first_name;
 }
 public String getLastName() {
  return lastName;
 }
 public void setLastName(String last_name) {
  this.lastName = last_name;
 }
 
 public String getMaidenName() {
	  return maidenName;
	 }
public void setMaidenName(String maidenName) {
	  this.lastName = maidenName;
	 }
public String getPosition() {
	  return position;
	 }
public void setPosition(String position) {
	  this.lastName = position;
	 }

 
 @Column(unique = true, nullable = false)
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getIndexNumber() {
		return indexNumber;
	}
	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getLinkedinLogin() {
		return linkedinLogin;
	}
@Column(unique = true, nullable = false)
	public void setLinkedinLogin(String linkedinLogin) {
		this.linkedinLogin = linkedinLogin;
	}
public String getStudentStatus() {
	return studentStatus;
}
public void setStudentStatus(String studentStatus) {
	this.studentStatus = studentStatus;
}
 
 
}
