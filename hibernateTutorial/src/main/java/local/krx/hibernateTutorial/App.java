package local.krx.hibernateTutorial;

import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class App 
{
    public static void main( String[] args )
    {
    	AnnotationConfiguration config = new AnnotationConfiguration();
        config.addAnnotatedClass(Osoba.class);
        config.configure("hibernate.cfg.xml");
        new SchemaExport(config).create(true, true);  
    }
}
