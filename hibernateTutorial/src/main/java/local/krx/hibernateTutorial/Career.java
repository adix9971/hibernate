package local.krx.hibernateTutorial;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Career {
	
	@Id
	 @GeneratedValue
	 private int id;
	 private String headline;
	 private String position;
	 private String dateWorkStart;
	 private String dateEndWork;
	 private String certyfications;
	 private String publications;
	 private String patents;
	 private String specialties;
	 
	 public int getId(){
		 return id;
		  }
	 public String getHeadline(){
		 return headline;
	 }
		
	 public String getPosition(){
		 return position;
	 }
	 
	 public String getdateWorkStart(){
		 return dateWorkStart;
	 }
	 
	 public String dateEndWork(){
		 return dateWorkStart;
	 }
	 
	 public void setid(int id) {
		  this.id = id;
	 }
	 
	 public void setHeadline(String headline){
		 this.headline=headline;
	 }
	 
	 public void setPosition(String position){
		 this.position=position;
		 
	 }
	public String getDateEndWork() {
		return dateEndWork;
	}
	public void setDateEndWork(String dateEndWork) {
		this.dateEndWork = dateEndWork;
	}
	public String getCertyfications() {
		return certyfications;
	}
	public void setCertyfications(String certyfications) {
		this.certyfications = certyfications;
	}
	public String getPublications() {
		return publications;
	}
	public void setPublications(String publications) {
		this.publications = publications;
	}
	public String getPatents() {
		return patents;
	}
	public void setPatents(String patents) {
		this.patents = patents;
	}
	public String getSpecialties() {
		return specialties;
	}
	public void setSpecialties(String specialties) {
		this.specialties = specialties;
	}
	}
	


